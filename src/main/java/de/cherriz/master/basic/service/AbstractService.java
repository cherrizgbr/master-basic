package de.cherriz.master.basic.service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.ext.MessageBodyReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Gemeinsame Basisfunktionen für REST Clients.
 *
 * @author Frederik Kirsch
 */
public class AbstractService {

    private Client client = null;

    private String url = null;

    private List<Class<? extends MessageBodyReader>> messageBodyReader = null;

    /**
     * Setzt die URL des Services.F
     *
     * @param url URL
     */
    public void setURL(String url) {
        this.url = url;
    }

    /**
     * Liefert das WebTarget für einen REST-Funktion.
     *
     * @param path Der Pfad zur Funktion.
     * @return Das WebTarget.
     */
    protected WebTarget getTarget(String path) {
        if (this.client == null) {
            ClientBuilder clientBuilder = ClientBuilder.newBuilder();

            if (this.messageBodyReader != null) {
                for (Class<? extends MessageBodyReader> reader : this.messageBodyReader) {
                    clientBuilder.register(reader);
                }
            }

            this.client = clientBuilder.build();
        }
        return client.target(this.url).path(path);
    }

    /**
     * Registriert einen MessageBodyReader,
     *
     * @param messageBodyReader
     */
    protected void addMessageBodyReader(Class<? extends MessageBodyReader> messageBodyReader) {
        if (this.messageBodyReader == null) {
            this.messageBodyReader = new ArrayList<Class<? extends MessageBodyReader>>();
        }
        this.messageBodyReader.add(messageBodyReader);
    }

}