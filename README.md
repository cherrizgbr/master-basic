# master-basic #

Dieses Repository ist Teil des Prototyps einer Masterarbeit zum Thema:  
*"Konzeption und prototypische Umsetzung der Integration fachlich getriebener Prozessmodellierung in die Softwareentwicklung unter Verwendung der Business Process Model and Notation 2.0"*  

### Informationen zum Modul ###

Das Modul stellt Basisfunktionalitäten für REST Clients zur Verfügung. Es liegt als Bibliothek verschiedenen Teilen des Prototyps bei.

### Weiterführende Übersicht ###

Diese Bibliothek ist Teil der im Architekturschaubild blau dargestellten, serverseitigen Spring Boot Services. Weiterführende Informationen, Zugang zur Arbeit sowie den anderen Modulen des Prototyps finden Sie auf einer eigenen [Übersichtsseite](www.cherriz.de/master) ([cherriz.de/master](www.cherriz.de/master)).  
![Architekturschaubild Prototyp](http://www.cherriz.de/master/images/content/architektur.png "Architekturschaubild Prototyp")

### Kontakt ###
[www.cherriz.de](www.cherriz.de)  
[www.cherriz.de/master](www.cherriz.de/master)  
[master@cherriz.de](mailto:master@cherriz.de)  


